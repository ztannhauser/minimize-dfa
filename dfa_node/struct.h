
#include <inttypes.h>
#include <wchar.h>
#include <stdbool.h>

struct dfa_node
{
	wchar_t* name; // must be first
	
	uint8_t* utf8_name;
	
	bool is_accepting;
	
	unsigned id;
	
	struct {
		struct dfa_transition {
			unsigned value;
			struct dfa_node* transition_to;
		}** data;
		size_t n, cap;
	} transitions;
	
	struct dfa_node* default_transition_to;
	
	enum {
		s_initialized,
		
		s_validated,
		
		s_flattened,
		
		s_rerouted,
		
		s_serialized_phase_0,
		s_serialized_phase_1,
		s_serialized_phase_2,
		s_serialized_phase_3,
	} state;
};

