
#include <debug.h>

#include <memory/gmalloc.h>
#include <memory/gneeds.h>
#include <memory/ginc.h>
#include <memory/gfree.h>

#include "struct.h"
#include "new.h"

void delete_dfa_node(void* ptr)
{
	ENTER;
	
	dpv(ptr);
	
	struct dfa_node* this = ptr;
	
	gfree(this->transitions.data);
	
	EXIT;
}

int new_dfa_node(struct dfa_node** retval, wchar_t* name)
{
	int error = 0;
	struct dfa_node* this = NULL;
	ENTER;
	
	error = gmalloc((void**) &this, sizeof(*this), delete_dfa_node);
	
	this->is_accepting = false;
	
	if (!error)
		error = gneeds(this, name);
	
	if (!error)
	{
		this->name = name;
		
		this->utf8_name = NULL;
		
		this->state = s_initialized;
		
		this->id = 0;
		
		this->default_transition_to = NULL;
		
		this->transitions.data = NULL;
		this->transitions.cap = 0;
		this->transitions.n = 0;
	}
	
	if (!error)
		*retval = ginc(this);
	
	gfree(this);
	
	EXIT;
	return error;
}

















