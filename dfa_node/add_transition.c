
#include <debug.h>

#include <macros/cmp.h>

#include <memory/gmalloc.h>
#include <memory/grealloc.h>
#include <memory/gneeds.h>
#include <memory/gfree.h>

#include "struct.h"
#include "add_transition.h"

static int compare(const void* a, const void* b)
{
	int cmp = 0;
	ENTER;
	
	cmp = cmp(
		((struct dfa_transition**) a)[0]->value,
		((struct dfa_transition**) b)[0]->value);
	
	dpv(cmp);
	
	EXIT;
	return cmp;
}

int dfa_node_add_transition(struct dfa_node* from, int value, struct dfa_node* transition_to)
{
	int error = 0;
	struct dfa_transition* transition = NULL;
	ENTER;
	
	dpv(transition);
	
	error = gmalloc((void**) &transition, sizeof(*transition), NULL);
	
	if (!error)
		error = gneeds(transition, transition_to);
	
	if (!error)
	{
		transition->value = value;
		transition->transition_to = transition_to;
	}
	
	if (!error)
		error = gneeds(from, transition);
	
	if (!error && from->transitions.n + 1 > from->transitions.cap)
		error = grealloc((void**) &from->transitions.data, sizeof(from->transitions.data[0]) * (from->transitions.cap = from->transitions.cap * 2 ?: 1));
	
	if (!error)
		from->transitions.data[from->transitions.n++] = transition;
	
	if (!error)
		qsort(from->transitions.data, from->transitions.n, sizeof(from->transitions.data[0]), compare);
	
	gfree(transition);
	
	EXIT;
	return error;
}
















