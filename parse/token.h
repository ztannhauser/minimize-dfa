
enum token
{
	t_error = 0,
	t_EOF,
	
	t_arrow,
	t_colon,
	t_comma,
	t_ellipsis,
	t_asterick,
	t_semicolon,
	t_exclamation,
	
	t_literal,
	t_identifier,
	
	number_of_tokens,
};

