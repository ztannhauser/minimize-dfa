
#include <debug.h>

#include <utf8/how_many.h>
#include <utf8/decode.h>

#include "yychar.h"

int yychar(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar)
{
	int error = 0, how_many, i;
	uint8_t utf8[6];
	ENTER;
	
	// claim the first byte:
	utf8[0] = *byte;
	
	// read next:
	error = yybyte(byte);
	
	// any more?
	if (!error)
		error = utf8_how_many(&how_many, utf8[0]);
	
	// read more:
	for (i = 1; !error && i < how_many; i++)
	{
		utf8[i] = *byte;
		error = yybyte(byte);
	}
	
	// decode:
	if (!error)
		error = utf8_decode(wchar, utf8);
	
	dpv(error);
	dpv(*wchar);
	
	EXIT;
	return error;
}













