
#include <stdio.h>
#include <avl.h>
#include <errno.h>

#include <error_codes.h>
#include <debug.h>

#include <memory/ginc.h>
#include <memory/gneeds.h>
#include <memory/delete.h>

#include <dfa_node/struct.h>
#include <dfa_node/new.h>
#include <dfa_node/add_transition.h>

#include "../token.h"
#include "../token_data.h"

#include "../yylex/yylex.h"

#include "yyparse.h"

static int compare(const void* a, const void* b)
{
	return wcscmp( *((wchar_t**) a), *((wchar_t**) b));
}

int yyparse(int (*yybyte)(uint8_t* byte), uint8_t* byte, wchar_t* wchar,
	enum token* token, union token_data* token_data, struct dfa_node** retval)
{
	int error = 0;
	ENTER;
	
	struct avl_tree_t* tree = avl_alloc_tree(compare, delete);
	
	dpv(tree);
	
	if (!tree)
		error = e_out_of_memory;
	
	int get_node(struct dfa_node** retval, wchar_t* name)
	{
		int error = 0;
		struct dfa_node* dfa_node;
		ENTER;
		
		struct avl_node_t* avl_node = avl_search(tree, &name);
		
		dpv(avl_node);
		
		if (avl_node)
		{
			*retval = ginc(avl_node->item);
		}
		else
		{
			// if you can't find it, make one
			error = new_dfa_node(&dfa_node, name);
			
			if (!avl_insert(tree, dfa_node))
				error = e_out_of_memory;
			
			if (!error)
				*retval = ginc(dfa_node);
		}
		
		EXIT;
		return error;
	}
	
	int read_transition(struct dfa_node* node)
	{
		int error = 0;
		bool is_default_transition = false;
		bool is_ranged = false;
		unsigned literal, upto_literal;
		ENTER;
		
		// better be a literal:
		if (*token == t_literal)
			literal = token_data->literal;
		else if (*token == t_asterick)
			is_default_transition = true;
		else
			error = e_syntax_error;
		
		// next?
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_ellipsis)
		{
			is_ranged = true;
			error = yylex(yybyte, byte, wchar, token, token_data);
			
			if (!error && *token != t_literal)
				error = e_syntax_error;
			
			if (!error)
				upto_literal = token_data->literal;
			
			if (!error)
				error = yylex(yybyte, byte, wchar, token, token_data);
		}
		
		if (!error && is_default_transition && is_ranged)
			error = e_syntax_error;
		
		// better be an '->'!
		if (!error && *token != t_arrow)
			error = e_syntax_error;
		
		// next?
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
			
		// better be a identifier!
		if (!error && *token != t_identifier)
			error = e_syntax_error;
		
		HERE;
		dpv(error);
		
		wchar_t* identifier = NULL;
		
		struct dfa_node* transition_to = NULL;
		if (!error)
		{
			identifier = token_data->identifier;
			error = get_node(&transition_to, identifier);
		}
		
		HERE;
		dpv(error);
		
		dpv(node);
		dpv(transition_to);
		
		if (!error)
		{
			if (is_default_transition)
			{
				delete(node->default_transition_to);
				node->default_transition_to = transition_to;
				error = gneeds(node, transition_to);
			}
			else if (is_ranged)
			{
				while (!error && literal - 1 <= upto_literal - 1)
					error = dfa_node_add_transition(node, literal++, transition_to);
			}
			else
			{
				error = dfa_node_add_transition(node, literal, transition_to);
			}
		}
		
		if (error)
			CHECK;
		
		// next?
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (error)
			CHECK;
		
		// might be a comma!
		if (!error && *token == t_comma)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (error)
			CHECK;
		
		delete(transition_to);
		delete(identifier);
		
		EXIT;
		return error;
	}
	
	struct dfa_node* starting_node = NULL;
	unsigned id = 0;
	
	int read_node()
	{
		int error = 0;
		struct dfa_node* node = NULL;
		bool is_starting_state = false;
		bool is_accepting = false;
		wchar_t* identifier = NULL;
		unsigned my_id = 0;
		ENTER;
		
		// read decorations:
		if (*token == t_arrow)
			is_starting_state = true,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_asterick)
			is_accepting = true,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (!error && *token == t_exclamation)
			my_id = id++,
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		// identifier:
		if (!error && *token != t_identifier)
			error = e_syntax_error;
		
		// fetch node with this name
		if (!error)
		{
			identifier = token_data->identifier;
			error = get_node(&node, identifier);
		}
		
		dpv(identifier);
		dpv(error);
		
		dpv(node);
		
		// save starting state:
		if (is_starting_state)
		{
			delete(starting_node);
			starting_node = ginc(node);
		}
		
		// save accepting state:
		if (!error)
		{
			if (is_accepting)
				node->is_accepting = is_accepting;
			
			if (my_id)
				node->id = my_id;
			
			// moving on...
			error = yylex(yybyte, byte, wchar, token, token_data);
		}
		
		// expect colon:
		if (!error && *token != t_colon)
			error = e_syntax_error;
		
		// moving on...
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		while (!error && *token != t_semicolon)
		{
			error = read_transition(node);
		}
		
		// eat ';'
		if (!error)
			error = yylex(yybyte, byte, wchar, token, token_data);
		
		if (error)
			CHECK;
		
		dpv(error);
		
		HERE;
		
		delete(node);
		
		HERE;
		delete(identifier);
		
		EXIT;
		return error;
	}
	
	dpv(error);
	
	while (!error && *token != t_EOF)
		error = read_node();
	
	if (error)
		CHECK;
	
	dpv(error);
	dpv(starting_node);
	
	if (!error && !starting_node)
	{
		fprintf(stderr, "starting node not specified!\n");
		error = e_syntax_error;
	}
	
	dpv(error);
	
	if (!error)
		*retval = ginc(starting_node);
	
	if (tree)
		avl_free_tree(tree);
	
	delete(starting_node);
	
	EXIT;
	return error;
}


















