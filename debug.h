
#define D(...) __VA_ARGS__
#define ND(...)

#if DEBUGGING
	#include <assert.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <search.h>
	#include <unistd.h>
	#include <wchar.h>
	#include <errno.h>
	#include <stdarg.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <inttypes.h>
	#include <fcntl.h>
	#include <stdbool.h>

	extern int debug_depth;
	
	#define TODO \
	{\
		assert(debug_depth >= 0); \
		printf("%*sTODO: File: %s: Line: %i\n", debug_depth, "", \
			__FILE__, __LINE__);\
		char buffer[100];\
		sprintf(buffer, "+%i", __LINE__);\
		execlp("gedit", "gedit", __FILE__, buffer, NULL);\
		assert(0);\
	}
	
	#define CHECK TODO
	#define NOPE CHECK
	#define HERE \
		printf("%*sHERE: File: %s: Line: %i\n", debug_depth, "", __FILE__, __LINE__);
	
	#define ENTER \
		assert(debug_depth >= 0), \
		printf("%*s<%s>\n", debug_depth++, "", __PRETTY_FUNCTION__);
	
	#define EXIT \
		assert(--debug_depth >= 0), \
		printf("%*s</%s>\n", debug_depth, "", __PRETTY_FUNCTION__);
	
	#define dprintf(format, ...) \
		assert(debug_depth >= 0),\
		printf("%*s" format, debug_depth, "", __VA_ARGS__)

	#define dpv(val) dprint(val)
	
	#define dpvb(b) \
		assert(debug_depth >= 0),\
		printf("%*s" #b " == %s\n", debug_depth, "", (b) ? "true" : "false");
	
	#define dpvc(ch) \
		assert(debug_depth >= 0),\
		printf("%*s" #ch " == '%c'\n", debug_depth, "", ch);
	
	#define dpvs(str) \
		assert(debug_depth >= 0),\
		printf("%*s" #str " == \"%s\"\n", debug_depth, "", str);
	
	#define dpvsn(str, len) \
		assert(debug_depth >= 0),\
		printf("%*s" #str " == \"%.*s\"\n", debug_depth, "", (int) len, str);
	
	#define dpvx(val) \
		assert(debug_depth >= 0),\
		printf((_Generic(val, \
			signed char:    "%*s" #val " == " "(signed char) (%#hhx)\n", \
			unsigned char:  "%*s" #val " == " "(unsigned char) (%#hhx)\n", \
			signed short:   "%*s" #val " == " "(signed short) (%#hx)\n", \
			unsigned short: "%*s" #val " == " "(unsigned char) (%#hx)\n", \
			signed int:     "%*s" #val " == " "(signed int) (%#x)\n", \
			unsigned int:   "%*s" #val " == " "(unsigned int) (%#x)\n", \
			signed long:    "%*s" #val " == " "(signed long) (%#lx)\n", \
			unsigned long:  "%*s" #val " == " "(unsigned long) (%#lx)\n", \
			float:          "%*s" #val " == " "(float) (%#a)\n", \
			double:         "%*s" #val " == " "(double) (%#la)\n", \
			long double:    "%*s" #val " == " "(long double) (%#La)\n", \
			default:        "%*s" #val " == " "(void*) (%#p)\n")), \
			debug_depth, "", val);
	
	#define dprint(val) \
		assert(debug_depth >= 0),\
		printf((_Generic(val, \
			signed char:    "%*s" #val " == " "(signed char) (%hhi)\n", \
			unsigned char:  "%*s" #val " == " "(unsigned char) (%hhu)\n", \
			signed short:   "%*s" #val " == " "(signed short) (%hi)\n", \
			unsigned short: "%*s" #val " == " "(unsigned short) (%hu)\n", \
			signed int:     "%*s" #val " == " "(signed int) (%i)\n", \
			unsigned int:   "%*s" #val " == " "(unsigned int) (%u)\n", \
			signed long:    "%*s" #val " == " "(signed long) (%li)\n", \
			unsigned long:  "%*s" #val " == " "(unsigned long) (%lu)\n", \
			float:          "%*s" #val " == " "(float) (%f)\n", \
			double:         "%*s" #val " == " "(double) (%lf)\n", \
			long double:    "%*s" #val " == " "(long double) (%Lf)\n", \
			default:        "%*s" #val " == " "(void*) (%p)\n")), \
			debug_depth, "", val);
#else
	#include <assert.h>
	
	#define TODO assert(!"TODO");
	#define CHECK assert(!"CHECK");
	#define NOPE assert(!"NOPE");
	#define dpvs(x) ;
	#define dpv(x) ;
	#define dpvb(x) ;
	#define dpvc(x) ;
	#define ENTER
	#define EXIT
	#define HERE ;
#endif

















