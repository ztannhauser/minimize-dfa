#include <stdbool.h>
extern const struct minimized_dfa_struct {
	enum state {
		s_error,
		s_A,
		s_B,
		s_C,
		number_of_states
	} start, table[number_of_states][50];
	bool accepting[number_of_states];
} minimized_dfa;

const struct minimized_dfa_struct minimized_dfa = {
	.start = s_A,
	.table = {
		[s_A] = {['0'] = s_B, ['1'] = s_B, },
		[s_B] = {['0'] = s_C, ['1'] = s_C, },
		[s_C] = {['0'] = s_A, ['1'] = s_B, },
	},
	.accepting = {
		[s_C] = true,
	}
};
