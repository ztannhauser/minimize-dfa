

# $ sudo apt install libavl-dev

TARGET = release

CC = gcc

CPPFLAGS += -I .

ifeq ($(TARGET), release)
CPPFLAGS += -D DEBUGGING=0
else
CPPFLAGS += -D DEBUGGING=1
endif

CFLAGS += -Wall -Werror

ifeq ($(TARGET), release)
CFLAGS += -O2
CFLAGS += -flto
else
CFLAGS += -g
CFLAGS += -Wno-unused-variable
CFLAGS += -Wno-unused-function
CFLAGS += -Wno-unused-but-set-variable
endif

LDLIBS += -lavl

default: bin/minimize-dfa.$(TARGET)

bin:
	rm -f ./bin
	ln -s `mktemp -d` bin
	find -type d | sed 's ^ bin/ ' | xargs -d \\n mkdir -p

bin/srclist.mk: | bin
	find -name '*.c' ! -name '*-out.c' | sed 's/^/SRCS += /g' > $@

include bin/srclist.mk

OBJS = $(patsubst %.c,bin/%.$(TARGET).o,$(SRCS))
DEPS = $(patsubst %.c,bin/%.$(TARGET).d,$(SRCS))

#ARGS += ./example1.dfa -o ./example1-out.c
ARGS += ./example2.dfa -o ./example2-out.c

run: bin/minimize-dfa.$(TARGET)
	$< $(ARGS)

valrun: bin/minimize-dfa.$(TARGET)
	valgrind $< $(ARGS)

install: ~/bin/minimize-dfa

~/bin/minimize-dfa: bin/minimize-dfa.release
	cp -vf $< $@

#/tmp/digraph.png: /tmp/digraph.gv
#	dot /tmp/digraph.gv -Tpng > /tmp/digraph.png

bin/minimize-dfa.$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS) -o $@

bin/%.$(TARGET).o bin/%.$(TARGET).d: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -MMD -o bin/$*.$(TARGET).o || (gedit $<; false)

include $(DEPS)


















