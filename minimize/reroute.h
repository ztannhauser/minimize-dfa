
#include <stdlib.h>
#include <stdbool.h>

#include "status.h"

struct dfa_node;

int reroute(
	struct dfa_node** nodes, size_t n,
	enum status (*statuses)[n][n],
	size_t index);
