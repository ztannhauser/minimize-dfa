
struct dfa_node;

int flatten(struct dfa_node*** nodes, size_t* n, struct dfa_node* start_node);
