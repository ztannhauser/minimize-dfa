
#include "status.h"

#if DEBUGGING
const char* status_names[number_of_statuses] = {
	[s_dont_know] = "s_dont_know",
	[s_definitely_different] = "s_definitely_different",
	[s_definitely_same] = "s_definitely_same",
};
#endif

