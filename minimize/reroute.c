
#include <debug.h>

#include <macros/min.h>
#include <macros/max.h>

#include <memory/gchangeneed.h>

#include <dfa_node/struct.h>

#include "macros/index.h"

#include "get_index.h"
#include "reroute.h"

int reroute(
	struct dfa_node** nodes, size_t n,
	enum status (*statuses)[n][n],
	size_t index)
{
	int error = 0;
	size_t i, m;
	size_t before_index, after_index;
	struct dfa_transition* transition;
	struct dfa_node* node;
	ENTER;
	
	node = nodes[index];
	
	if (node->state != s_rerouted)
	{
		node->state = s_rerouted;
		
		dpv(index);
		
		// for each transition:
		for (i = 0, node = nodes[index], m = node->transitions.n; !error && i < m; i++)
		{
			transition = node->transitions.data[i];
			
			before_index = get_index(nodes, n, transition->transition_to);
			
			// get index of connected node
			dpv(before_index);
			
			// get *lowest* index of equalivent node
			for (after_index = 0; after_index < before_index && index(before_index, after_index) != s_definitely_same; after_index++);
			
			dpv(after_index);
			
			// call reroute(lowest node)
			error = reroute(nodes, n, statuses, after_index);
			
			// node.gchangeneed(from = connected node, to = lowest_node);
			if (!error)
				error = gchangeneed(transition, nodes[before_index], nodes[after_index]);
			
			// transition->transition_to = lowest_node
			transition->transition_to = nodes[after_index];
		}
		
		if (!error && node->default_transition_to)
		{
			before_index = get_index(nodes, n, node->default_transition_to);
			
			// get index of connected node
			dpv(before_index);
			
			// get *lowest* index of equalivent node
			for (after_index = 0; after_index < before_index && index(before_index, after_index) != s_definitely_same; after_index++);
			
			dpv(after_index);
			
			// call reroute(lowest node)
			error = reroute(nodes, n, statuses, after_index);
			
			// node.gchangeneed(from = connected node, to = lowest_node);
			if (!error)
				error = gchangeneed(node, nodes[before_index], nodes[after_index]);
			
			// transition->transition_to = lowest_node
			node->default_transition_to = nodes[after_index];
		}
	}
	EXIT;
	return error;
}















