
#include <search.h>

#include <stdlib.h>

#include "compare.h"
#include "get_index.h"

size_t get_index(struct dfa_node** nodes, size_t n, struct dfa_node* findme)
{
	return (((struct dfa_node**) bsearch(&findme, nodes, n, sizeof(nodes[0]), compare)) - nodes);
}

