
#include <debug.h>

#include <memory/gmalloc.h>
#include <memory/gdigraph.h>
#include <memory/delete.h>

#include <macros/min.h>
#include <macros/max.h>

#include <dfa_node/struct.h>

#include "status.h"
#include "flatten.h"
#include "test.h"
#include "get_index.h"
#include "reroute.h"
#include "minimize.h"

int minimize(struct dfa_node* start_node)
{
	int error = 0;
	ENTER;
	
	size_t n;
	struct dfa_node** nodes = NULL;
	
	error = flatten(&nodes, &n, start_node);
	
	enum status (*statuses)[n][n] = NULL;
	
	if (!error)
		error = gmalloc((void**) &statuses, sizeof(*statuses), NULL);
	
	size_t i, j;
	
	if (!error)
	{
		for (i = 0; i < n; i++)
		{
			for (j = 0; j < i; j++)
			{
				test(nodes, n, statuses, i, j);
			}
		}
		
		error = reroute(nodes, n, statuses, 0); // 0 is the starting node
	}
	
/*	gdigraph(start_node);*/
/*	CEHCK;*/
	
	delete(statuses);
	delete(nodes);
	
	EXIT;
	return error;
}










