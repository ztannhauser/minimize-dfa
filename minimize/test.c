
#include <debug.h>

#include <macros/min.h>
#include <macros/max.h>

#include <dfa_node/struct.h>

#include "macros/index.h"

#include "get_index.h"
#include "test.h"

enum status test(
	struct dfa_node** nodes, size_t n_nodes,
	enum status (*statuses)[n_nodes][n_nodes],
	size_t a_index, size_t b_index)
{
	struct {
		size_t i, n;
		struct dfa_node* node;
		struct dfa_transition* t;
		size_t transition_to_index;
	} a, b;
	
	ENTER;
	
	dpv(a_index);
	dpv(b_index);
	
	a.node = nodes[a_index], b.node = nodes[b_index];
	
	dpvc(a.node->name[0]);
	dpvc(a.node->name[1]);
	dpvc(b.node->name[0]);
	dpvc(b.node->name[1]);
	
	enum status *status = &(index(a_index, b_index));
	
	dpvs(status_names[*status]);
	
	if (!*status)
	{
		*status = s_definitely_same;
		
		if (a.node == b.node)
			*status = s_definitely_same;
		else if (a.node->is_accepting != b.node->is_accepting)
			*status = s_definitely_different;
		else if (a.node->id != b.node->id)
			*status = s_definitely_different;
		else
		{
			for (
				a.i = 0, a.n = a.node->transitions.n,
				b.i = 0, b.n = b.node->transitions.n;
				*status == s_definitely_same && (a.i < a.n || b.i < b.n); )
			{
				a.t = a.i < a.n ? a.node->transitions.data[a.i] : NULL;
				b.t = b.i < b.n ? b.node->transitions.data[b.i] : NULL;
				
				if (a.t)
				{
					dpv(a.t->value);
					dpv(a.t->transition_to);
				}
				
				if (b.t)
				{
					dpv(b.t->value);
					dpv(b.t->transition_to);
				}
				
				if (a.t && (!b.t || a.t->value < b.t->value) && b.node->default_transition_to)
				{
					// a has a transition, b has default
					TODO;
					a.i++;
				}
				else if (b.t && (!a.t || b.t->value < a.t->value) && a.node->default_transition_to)
				{
					// b has a transition, a has default
					
					b.transition_to_index = get_index(nodes, n_nodes, b.t->transition_to);
					a.transition_to_index = get_index(nodes, n_nodes, a.node->default_transition_to);
					
					dpv(b.transition_to_index);
					dpv(a.transition_to_index);
					
					if (test(nodes, n_nodes, statuses, a.transition_to_index, b.transition_to_index) == s_definitely_different)
						*status = s_definitely_different;
					
					b.i++;
				}
				else if (a.t && b.t && a.t->value == b.t->value)
				{
					// both 'a' and 'b' have a transition on the same value
					a.transition_to_index = get_index(nodes, n_nodes, a.t->transition_to);
					b.transition_to_index = get_index(nodes, n_nodes, b.t->transition_to);
					
					dpv(a.transition_to_index);
					dpv(b.transition_to_index);
					
					if (test(nodes, n_nodes, statuses, a.transition_to_index, b.transition_to_index) == s_definitely_different)
						*status = s_definitely_different;
					
					a.i++, b.i++;
				}
				else
				{
					*status = s_definitely_different;
				}
			}
			
			if (*status == s_definitely_same && a.node->default_transition_to && b.node->default_transition_to)
			{
				a.transition_to_index = get_index(nodes, n_nodes, a.node->default_transition_to);
				b.transition_to_index = get_index(nodes, n_nodes, b.node->default_transition_to);
				
				dpv(a.transition_to_index);
				dpv(b.transition_to_index);
				
				if (test(nodes, n_nodes, statuses, a.transition_to_index, b.transition_to_index) == s_definitely_different)
					*status = s_definitely_different;
			}
		}
		
		HERE;
		dpvs(status_names[*status]);
	}
	
	EXIT;
	return *status;
}














