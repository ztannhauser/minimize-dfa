
#include <search.h>

#include <debug.h>

#include <memory/grealloc.h>
#include <memory/gneeds.h>
#include <memory/gdigraph.h>
#include <memory/ginc.h>
#include <memory/delete.h>

#include <dfa_node/struct.h>

#include <traverse.h>

#include "compare.h"
#include "flatten.h"

int flatten(
	struct dfa_node*** outgoing_nodes, size_t* outgoing_n,
	struct dfa_node* start_node)
{
	int error = 0;
	ENTER;
	
	struct dfa_node** nodes = NULL;
	size_t n = 0, cap = 0;
	
	int func(struct dfa_node* node)
	{
		int error = 0;
		ENTER;
		
		if (n + 1 >= cap)
			error = grealloc((void**) &nodes, sizeof(*nodes) * (cap = cap * 2 ?: 1));
		
		if (!error)
			error = gneeds(nodes, node);
		
		if (!error)
			nodes[n++] = node;
			
		EXIT;
		return error;
	}
	
	error = traverse(start_node, s_flattened, func);
	
/*	gdigraph(nodes);*/
	
	if (!error)
		qsort(nodes, n, sizeof(nodes[0]), compare);
	
	if (!error)
	{
		*outgoing_nodes = ginc(nodes);
		*outgoing_n = n;
	}
	
	delete(nodes);
	
	EXIT;
	return error;
}















