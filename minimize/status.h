
#ifndef ENUM_STATUS
#define ENUM_STATUS

enum status {
	s_dont_know,
	s_definitely_different,
	s_definitely_same,
	number_of_statuses
};

#if DEBUGGING
extern const char* status_names[number_of_statuses];
#endif

#endif
