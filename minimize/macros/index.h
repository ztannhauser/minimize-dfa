
#include <macros/min.h>
#include <macros/max.h>

#define index(i, j) (*statuses)[min(i, j)][max(i, j)]
