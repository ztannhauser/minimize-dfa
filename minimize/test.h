
#include <stdlib.h>

#include "status.h"

struct dfa_node;

enum status test(
	struct dfa_node** nodes, size_t n_nodes,
	enum status (*statuses)[n_nodes][n_nodes],
	size_t ai, size_t bi);
