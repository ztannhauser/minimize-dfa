
#include <stdlib.h>

#include <debug.h>

#include <dfa_node/struct.h>

#include "traverse.h"

int traverse(struct dfa_node* start_node, int state, int (*callback)(struct dfa_node* node))
{
	int func(struct dfa_node* node)
	{
		int error = 0;
		size_t i, n;
		
		if (node->state != state)
		{
			node->state = state;
			
			error = callback(node);
			
			for (i = 0, n = node->transitions.n; !error && i < n; i++)
				error = func(node->transitions.data[i]->transition_to);
			
			if (!error && node->default_transition_to)
				error = func(node->default_transition_to);
		}
		
		return error;
	}
	
	return func(start_node);
}

