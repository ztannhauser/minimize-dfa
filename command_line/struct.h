
#include <stdbool.h>

struct cmdln_flags
{
	const char* infile;
	const char* outfile;
	const char* dfa_name;
	const char* state_prefix;
	bool write_header;
};

