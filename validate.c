
#include <stdio.h>

#include <error_codes.h>
#include <debug.h>

#include <dfa_node/struct.h>

#include "traverse.h"
#include "validate.h"

int validate(struct dfa_node* start_node)
{
	int error = 0;
	ENTER;
	
	error = traverse(start_node, s_validated, ({
		int callback(struct dfa_node* node)
		{
			int error = 0;
			unsigned now, then;
			size_t i, n;
			ENTER;
			
			for (i = 0, n = node->transitions.n; !error && i < n; i++, then = now)
			{
				now = node->transitions.data[i]->value;
				
				if (i > 0 && now == then)
				{
					fprintf(stderr, "not a DFA.\n");
					error = e_syntax_error;
				}
			}
			
			EXIT;
			return error;
		}
		callback;
	}));
	
	EXIT;
	return error;
}

