
#include <debug.h>

#include "encode.h"
#include "encode_string.h"

void utf8_encode_string(uint8_t* utf8_out, wchar_t* unicode_in)
{
	ENTER;
	
	for (; *unicode_in; unicode_in++)
		utf8_out += utf8_encode(utf8_out, *unicode_in);
	
	*utf8_out = '\0';
	
	EXIT;
}

