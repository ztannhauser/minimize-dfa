
struct dfa_node;

int serialize(
	struct dfa_node* start_node,
	const char* path,
	bool write_header,
	const char* dfa_name,
	const char* state_prefix);
