
#include <error_codes.h>
#include <debug.h>
#include <string.h>
#include <stdio.h>
#include <linux/limits.h>

#include <macros/max.h>

#include <memory/gmalloc.h>
#include <memory/gneeds.h>
#include <memory/delete.h>

#include <dfa_node/struct.h>

#include <utf8/encode_string.h>

#include <traverse.h>

#include "serialize.h"

int serialize(
	struct dfa_node* start_node,
	const char* path,
	bool write_header,
	const char* dfa_name,
	const char* state_prefix)
{
	int error = 0;
	ENTER;
	
	if (!path)
		error = e_bad_command_line_args;
	
	FILE* fout = NULL;
	
	if (!error && !(fout = fopen(path, "w+")))
		perror("fopen"),
		error = e_syscall_failed;
	
	FILE* hout = NULL;
	char header_path[PATH_MAX];
	if (!error && write_header)
	{
		strcpy(header_path, path);
		strcpy(rindex(header_path, '.'), ".h");
		
		dpvs(header_path);
		
		if (!(hout = fopen(header_path, "w+")))
			perror("fopen"),
			error = e_syscall_failed;
	}
	
	unsigned highest_transition_value = 0;
	
	// make utf8 names for nodes, and find highest_transition_value:
	error = traverse(start_node, s_serialized_phase_0, ({
		int callback(struct dfa_node* node)
		{
			int error = 0;
			size_t i, n;
			
			for (i = 0, n = node->transitions.n; i < n; i++)
				highest_transition_value = max(highest_transition_value, node->transitions.data[i]->value);
			
			uint8_t* utf8_name = NULL;
			size_t strlen = wcslen(node->name);
			
			dpv(strlen);
			
			error = gmalloc((void**) &utf8_name, (strlen + 1), NULL);
			
			if (!error)
			{
				utf8_encode_string(utf8_name, node->name);
				error = gneeds(node, utf8_name);
			}
			
			if (!error)
				node->utf8_name = utf8_name;
			
			dpvs(utf8_name);
			
			delete(utf8_name);
			return error;
		}
		callback;
	}));
	
	state_prefix = state_prefix ?: "s";
	dfa_name = dfa_name ?: "minimized_dfa";
	
	if (!error)
	{
		void write_type(FILE* fout)
		{
			fprintf(fout, "#include <stdbool.h>\n\n");
			
			fprintf(fout, "extern const struct %s_struct {\n", dfa_name);
			fprintf(fout, "\tenum state {\n");
			
			fprintf(fout, "\t\t%s_error,\n", state_prefix);
			
			int func1(struct dfa_node* node)
			{
				fprintf(fout, "\t\t%s_%s,\n", state_prefix, node->utf8_name);
				return 0;
			}
			
			traverse(start_node, s_serialized_phase_1, func1);
			
			fprintf(fout, "\t\tnumber_of_states\n");
			
			fprintf(fout, "\t} start, table[number_of_states][%u];\n", highest_transition_value + 1);
			
			fprintf(fout, "\tbool accepting[number_of_states];\n");
			
			fprintf(fout, "} %s;\n\n", dfa_name);
		}
		
		char* suffix;
		if (write_header)
		{
			suffix = rindex(header_path, '/');
			
			write_type(hout);
			
			fprintf(fout, "#include \"%s\"\n", suffix ? suffix + 1 : header_path);
		}
		else
		{
			write_type(fout);
		}
		
		fprintf(fout, "const struct %s_struct %s = {\n", dfa_name, dfa_name);
		
		fprintf(fout, "\t.start = %s_%s,\n", state_prefix, start_node->utf8_name);
		
		fprintf(fout, "\t.table = {\n");
		
		int func2(struct dfa_node* node)
		{
			size_t i, n;
			struct dfa_transition* transition;
			
			fprintf(fout, "\t\t[%s_%s] = {", state_prefix, node->utf8_name);
			
			if (node->default_transition_to)
			{
				fprintf(fout, "[0 ... %u] = %s_%s, ", highest_transition_value, state_prefix, node->default_transition_to->utf8_name);
			}
			
			for (i = 0, n = node->transitions.n; i < n; i++)
			{
				transition = node->transitions.data[i];
				
				if (transition->value < 127 && index(
					"1234567890"
					"ABCDEFGHIJKLMNNOPQRSTUVWXYZ"
					"abcdefghijklmnnopqrstuvwxyz",
					transition->value))
				{
					fprintf(fout, "['%c']", transition->value);
				}
				else
				{
					fprintf(fout, "[%u]", transition->value);
				}
				
				fprintf(fout, " = %s_%s, ", state_prefix, transition->transition_to->utf8_name);
			}
			
			fprintf(fout, "},\n");
			
			return 0;
		}
		
		traverse(start_node, s_serialized_phase_2, func2);
		
		fprintf(fout, "\t},\n");
		
		fprintf(fout, "\t.accepting = {\n");
		
		int func3(struct dfa_node* node)
		{
			if (node->is_accepting)
			{
				fprintf(fout, "\t\t[%s_%s] = true,\n", state_prefix, node->utf8_name);
			}
			
			return 0;
		}
		
		traverse(start_node, s_serialized_phase_3, func3);
		
		fprintf(fout, "\t}\n");
		
		fprintf(fout, "};\n");
	}
	
	if (fout)
		fclose(fout);
	
	if (hout)
		fclose(hout);
	
	EXIT;
	return error;
}



























