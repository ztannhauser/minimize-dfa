
struct dfa_node;

int traverse(struct dfa_node* start_node, int state, int (*callback)(struct dfa_node* node));
