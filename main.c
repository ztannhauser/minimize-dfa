
#include <stdio.h>

#include <debug.h>

/*#include <memory/gdigraph.h>*/

#include <command_line/struct.h>
#include <command_line/process_flags.h>

#include <parse/parse.h>

#include "validate.h"

#include <minimize/minimize.h>

#include <serialize/serialize.h>

#include <memory/delete.h>

#if DEBUGGING
int debug_depth;
#endif

int main(int argc, char *const * argv)
{
	int error = 0;
	struct cmdln_flags* flags = NULL;
	struct dfa_node* start_node = NULL;
	ENTER;
	
	error = 0
		?: process_flags(&flags, argc, argv)
		?: parse(&start_node, flags->infile)
		?: validate(start_node)
		?: minimize(start_node)
		?: serialize(start_node, flags->outfile, flags->write_header, flags->dfa_name, flags->state_prefix);
	
	HERE;
	
/*	gdigraph(start_node);*/
	
	delete(flags);
	delete(start_node);
	
	EXIT;
	return error;
}
























