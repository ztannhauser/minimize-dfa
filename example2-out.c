#include <stdbool.h>

extern const struct minimized_dfa_struct {
	enum state {
		s_error,
		s_A,
		s_B,
		s_C,
		number_of_states
	} start, table[number_of_states][91];
	bool accepting[number_of_states];
} minimized_dfa;

const struct minimized_dfa_struct minimized_dfa = {
	.start = s_A,
	.table = {
		[s_A] = {['0'] = s_B, ['1'] = s_B, ['2'] = s_B, ['3'] = s_B, ['4'] = s_B, ['5'] = s_B, ['6'] = s_B, ['7'] = s_B, ['8'] = s_B, ['9'] = s_B, ['A'] = s_C, ['B'] = s_C, ['C'] = s_C, ['D'] = s_C, ['E'] = s_C, ['F'] = s_C, ['G'] = s_C, ['H'] = s_C, ['I'] = s_C, ['J'] = s_C, ['K'] = s_C, ['L'] = s_C, ['M'] = s_C, ['N'] = s_C, ['O'] = s_C, ['P'] = s_C, ['Q'] = s_C, ['R'] = s_C, ['S'] = s_C, ['T'] = s_C, ['U'] = s_C, ['V'] = s_C, ['W'] = s_C, ['X'] = s_C, ['Y'] = s_C, ['Z'] = s_C, },
		[s_B] = {},
		[s_C] = {},
	},
	.accepting = {
		[s_B] = true,
		[s_C] = true,
	}
};
