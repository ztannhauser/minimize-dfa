
#include <debug.h>

#include "header.h"
#include "gmalloc.h"
#include "grealloc.h"

int grealloc(void** ptr, size_t newsize)
{
	int error = 0;
	size_t i, n;
	size_t j, m;
	void *oldptr, *newptr;
	struct gmemory_header *oldheader, *newheader;
	struct gmemory_header *tmpheader;
	ENTER;
	
	assert(ptr);
	
	if (*ptr)
	{
		oldptr = oldheader = *ptr - sizeof(*oldheader);
		
		dpv(newsize);
		dpv(oldheader->size);
		
		if (oldheader->size < newsize)
		{
			newptr = newheader = realloc(oldptr, sizeof(*newheader) + newsize);
			
			newheader->size = newsize;
			
			dpv(newheader->refcount);
			dpv(newheader->size);
			
			// contents have moved, update those who refer to me
			if (oldptr != newptr)
			{
				for (i = 0, n = newheader->thosewhoneedme.n; i < n; i++)
				{
					TODO;
				}
				
				for (i = 0, n = newheader->thoseineed.n; i < n; i++)
				{
					tmpheader = newheader->thoseineed.headers[i];
					
					for (j = 0, m = tmpheader->thosewhoneedme.n; j < m; j++)
						if (tmpheader->thosewhoneedme.headers[j] == oldheader)
							tmpheader->thosewhoneedme.headers[j] = newheader;
				}
			}
			
			*ptr = newptr + sizeof(*newheader);
		}
	}
	else
	{
		error = gmalloc(ptr, newsize, NULL);
	}
	
	EXIT;
	return error;
}










