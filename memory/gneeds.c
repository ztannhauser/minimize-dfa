
#include <debug.h>
#include <error_codes.h>

#include "header.h"
#include "gneeds.h"

int gneeds(void* inneed, void* needsme)
{
	int error = 0;
	struct gmemory_header* inneed_header;
	struct gmemory_header* needsme_header;
	struct gmemory_header **newheaders;
	ENTER;
	
	assert(inneed);
	assert(needsme);
	
	inneed_header  = inneed  - sizeof(*inneed_header);
	needsme_header = needsme - sizeof(*needsme_header);
	
	dpv(inneed_header->thoseineed.n);
	
	if (inneed_header->thoseineed.n + 1 >= inneed_header->thoseineed.cap)
	{
		newheaders = realloc(inneed_header->thoseineed.headers, sizeof(*newheaders) * (inneed_header->thoseineed.cap = inneed_header->thoseineed.cap * 2 ?: 1));
		
		if (!newheaders)
			error = e_out_of_memory;
		
		if (!error)
			inneed_header->thoseineed.headers = newheaders;
	}
	
	if (!error)
		inneed_header->thoseineed.headers[inneed_header->thoseineed.n++] = needsme_header;
	
	dpv(needsme_header->thosewhoneedme.n);
	
	if (needsme_header->thosewhoneedme.n + 1 >= needsme_header->thosewhoneedme.cap)
	{
		newheaders = realloc(needsme_header->thosewhoneedme.headers, sizeof(*newheaders) * (needsme_header->thosewhoneedme.cap = needsme_header->thosewhoneedme.cap * 2 ?: 1));
		
		if (!newheaders)
			error = e_out_of_memory;
		
		if (!error)
			needsme_header->thosewhoneedme.headers = newheaders;
	}
	
	if (!error)
		needsme_header->thosewhoneedme.headers[needsme_header->thosewhoneedme.n++] = inneed_header;
	
	dpv(inneed_header->thoseineed.n);
	dpv(needsme_header->thosewhoneedme.n);
	
	EXIT;
	return error;
}
































