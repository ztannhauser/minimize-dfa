
#include <string.h>

#include <error_codes.h>
#include <debug.h>

#include "header.h"
#include "gmalloc.h"

int gmalloc(void** retval, size_t size, void (*deconstructor)(void*))
{
	int error = 0;
	ENTER;
	
	assert(retval);
	
	dpv(size);
	dpv(deconstructor);
	
	void* ptr = malloc(sizeof(struct gmemory_header) + size);
	
	if (!ptr)
		error = e_out_of_memory;
	
	struct gmemory_header* header = ptr;
	
	if (!error)
	{
		header->thoseineed.headers = NULL;
		header->thoseineed.n = 0;
		header->thoseineed.cap = 0;
		
		header->thosewhoneedme.headers = NULL;
		header->thosewhoneedme.n = 0;
		header->thosewhoneedme.cap = 0;
		
		header->status = s_default;
		header->size = size;
		header->refcount = 1;
		header->deconstructor = deconstructor;
		
		memset(header + 1, 0, size);
	}
	
	if (error)
		free(ptr);
	else
		*retval = header + 1;
	
	EXIT;
	return error;
}


















