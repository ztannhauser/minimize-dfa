
enum
{
	e_success = 0,
	e_syscall_failed,
	e_out_of_memory,
	e_malformed_utf8,
	e_bad_command_line_args,
	e_syntax_error,
};

